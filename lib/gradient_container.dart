import 'package:flutter/material.dart';

import 'dice_roller.dart';

const Alignment startAlign = Alignment.topLeft;
const Alignment endAlign = Alignment.bottomRight;
const List<Color> defColors = <Color>[
  Color.fromARGB(255, 26, 2, 80),
  Color.fromARGB(255, 45, 7, 98)
];

const List<Color> orangeColors = <Color>[
  Color.fromARGB(255, 25, 130, 0),
  Color.fromARGB(255, 255, 145, 0),
  Color.fromARGB(255, 255, 160, 0),
  Color.fromARGB(255, 255, 175, 0),
  Color.fromARGB(255, 255, 190, 0),
];

class GradientContainer extends StatelessWidget {
  const GradientContainer({super.key, this.colors = defColors});
  const GradientContainer.orange({super.key}) : colors = orangeColors;

  final List<Color> colors;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: colors,
          begin: startAlign,
          end: endAlign,
        ),
      ),
      child: const Center(
        child: DiceRoller(),
      ),
    );
  }
}

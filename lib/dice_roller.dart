import 'dart:math';

import 'package:flutter/material.dart';

import 'styled_text.dart';

final Random _randomizer = Random();

class DiceRoller extends StatefulWidget {
  const DiceRoller({super.key});

  @override
  State<DiceRoller> createState() => _DiceRollerState();
}

class _DiceRollerState extends State<DiceRoller> {
  int currDiceRoll = 6;

  void rollDice() {
    setState(() => currDiceRoll = _randomizer.nextInt(6) + 1);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Image.asset(
          'assets/images/dice-$currDiceRoll.png',
          width: 280.0,
        ),
        const SizedBox(height: 20.0),
        ElevatedButton(
          onPressed: rollDice,
          style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.all(16.0),
            foregroundColor: Colors.black,
          ),
          child: const StyledText(
            'roll a dice',
            color: Colors.black,
          ),
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';

class StyledText extends StatelessWidget {
  const StyledText(this.text, {key, this.color = Colors.white})
      : super(key: key);

  final String text;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 28.0,
        fontWeight: FontWeight.w400,
        color: color,
      ),
    );
  }
}
